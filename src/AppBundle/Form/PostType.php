<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
              'label'    => 'post.label.username',
              'required' => false
            ])
            ->add('message', TextareaType::class, [
              'label'    => 'post.label.message',
              'required' => false
            ])
            ->add('submit', SubmitType::class, [
              'label'    => 'post.create',
              'attr'     => [
                'class'  => 'btn btn-primary']
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Post',
            'timed_spam' => true,
            'timed_spam_min' => 3,
            'timed_spam_max' => 40,
            'timed_spam_message' => 'Please wait 3 seconds before create',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'form';
    }
}
