<?php

namespace AppBundle\Services;

class UserManagerService
{
    private $session;

    private $entity;

    public function termUser()
    {
        if ($this->session->has('user')) {
            return $this->session->get('user');
        } else {
            return null;
        }
    }

    public function canTermUser($user)
    {
        if (!$this->session->has('user')) {
            $this->session->set('user', $user);
        }
    }

    public function lastVisit()
    {
        $this->session->set('last_visit', new \DateTime());
    }

    public function logout()
    {
        $this->session->clear();
    }

    public function setSession($session)
    {
        $this->session = $session;
    }

    public function setEntityManager($entityManager)
    {
        $this->entity = $entityManager;
    }
}
