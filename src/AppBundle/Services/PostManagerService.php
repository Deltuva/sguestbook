<?php

namespace AppBundle\Services;

use AppBundle\Entity\Post;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PostManagerService
{
    private $container;

    private $entity = null;

    public function getPosts()
    {
        $bookRepo = $this->entity->getRepository(Post::class);
        $posts = $bookRepo->getPostsList();

        return self::paginator($posts);
    }

    public function countPosts()
    {
        return (int)$this->entity->getRepository(Post::class)->getCountPosts();
    }

    public function countPostsBy($params)
    {
        return (int)$this->entity->getRepository(Post::class)->getCountPostsBy($params);
    }

    public function paginator($posts, $page = 1)
    {
        $paginator = $this->container->get('knp_paginator');
        if ($this->container->has('request_stack')) {
            $req = $this->container->get('request_stack')->getCurrentRequest();
        }
        $paginate = $paginator->paginate(
            $posts,
            $req->query->getInt('page', $page), Post::POST_ITEMS
        );

        return $paginate;
    }

    public function create($post, $formData)
    {
        $um = $this->container->get('user.manager');
        $user = $um->termUser() ? $um->termUser() : $formData->getUsername();
        if (null !== $formData->getUsername()) {
            $post->setUsername($user);
        }
        if (!$um->termUser()) {
            $um->canTermUser($formData->getUsername());
        }
        if (null !== $formData->getMessage()) {
            $post->setMessage($formData->getMessage());
        }
        $post->setCreatedAt(new \DateTime());

        $entity = $this->entity;
        $entity->persist($post);
        $entity->flush();

        if ($entity != null) {
            return true;
        }
    }

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function setEntityManager(EntityManager $entity)
    {
        $this->entity = $entity;
    }
}
