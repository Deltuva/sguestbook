<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 */
class Post
{
    const POST_ITEMS = 10;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=25)
     * @Assert\NotBlank(message = "post.username")
     * @Assert\Length(
     *      min = 3,
     *      max = 25,
     *      minMessage = "post.username_characters_than",
     *      maxMessage = "post.username_characters_less"
     * )
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=500)
     * @Assert\NotBlank(message = "post.message")
     * @Assert\Length(
     *      min = 2,
     *      max = 500,
     *      minMessage = "post.message_characters_than",
     *      maxMessage = "post.message_characters_less"
     * )
     */
    private $message;

    /**
     * @var integer
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * [__construct description]
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Post
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @param string $username
     *
     * @return Post
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Post
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set created date
     *
     * @param string $createdAt
     *
     * @return datetime
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get created date
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
