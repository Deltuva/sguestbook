<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Post;
use AppBundle\Form\PostType;

class GuestbookController extends Controller
{
    /**
     * @Route("/", name="book")
     */
    public function indexAction()
    {
        $userService = $this->get('user.manager');
        $userService->lastVisit();
        // the container will instantiate a new PostManager()
        $bookService = $this->get('post.manager');
        $posts = $bookService->getPosts();
        $totalPosts = $bookService->countPosts();
        $totalPostsByUser = $bookService->countPostsBy(['user' => $userService->termUser()]);

        return $this->render('book/index.html.twig', ['posts' => $posts, 'total' => $totalPosts,
            'me_total' => $totalPostsByUser
          ]);
    }

    /**
     * @Route("/book/new", name="new_post")
     * @Method({"GET", "POST"})
     */
    public function newPostAction(Request $request)
    {
        $post = new Post;
        $post->setUsername($this->get('user.manager')->termUser());
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        $bookService = $this->get('post.manager');
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $created = $bookService->create($post, $data);

            if ($created == true) {
                return $this->redirectToRoute('book');
            }
        }

        return $this->render('book/form.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }
}
