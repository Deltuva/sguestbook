<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Post;

class AdminController extends Controller
{
    /**
       * Admin page
       * @Route("/admin")
       * @Security("is_granted('IS_AUTHENTICATED_FULLY') && has_role('ROLE_ADMIN')")
       */
        public function adminAction()
        {
            $user = $this->get('security.token_storage')->getToken()->getUser();

            return $this->render('admin/index.html.twig', ['user' => $user]);
        }

      /**
       * Delete a Post.
       *
       * @Route("/admin/delete/{id}", name="admin_post_delete")
       * @Security("is_granted('IS_AUTHENTICATED_FULLY') && has_role('ROLE_ADMIN')")
       * @Method("GET")
       *
       */
      public function deleteAction($id)
      {
          $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

          $entity = $this->getDoctrine()->getManager();
          $post = $entity->getRepository(Post::class)->findOneBy(['id' => $id]);
          if (!$post) {
              throw $this->createNotFoundException('No post found for id '.$id);
          }
          $entity->remove($post);
          $entity->flush();

          $this->addFlash('admin_post_deleted', 'admin.deleted_successfully');

          return $this->redirectToRoute('book');
      }

      /**
       * Logout.
       *
       * @Route("/admin/logout", name="admin_logout")
       * @Security("is_granted('IS_AUTHENTICATED_FULLY') && has_role('ROLE_ADMIN')")
       * @Method("GET")
       *
       */
      public function logoutAction()
      {
          $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
          $this->get('user.manager')->logout();

          return $this->redirectToRoute('book');
      }
}
